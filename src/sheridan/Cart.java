/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mauricio
 */
public class Cart {
    private List<Product> products;
    private PaymentService service;

    public Cart() {
        products = new ArrayList<Product>();
    }
    
    public void setPaymentService(PaymentService service) {
        this.service = service;
    }
    
    public void addProduct(Product product){
        products.add(product);
    }
    
    public void payCart(){
        double totalPrice=0;
        for(Product p: products){
           totalPrice += p.getPrice();
        }
        service.ProcessPayment(totalPrice);
    }
}
